# Simple Selenium Grid Without Selenium Hub.
> Trying haproxy instead of selenium hub

## 1.Build

### 1.1 Build Haproxy image.

```
docker build -t garinger/haproxy:1.9 .
```

### 1.2 Run docker containers
```
docker-compose up -d
```

### 1.3 Check status of docker containers
```
docker-compose ps
```

### 1.4 Run unit testing
cn.edu.sysu.garinger.WebDriverPoolTest
