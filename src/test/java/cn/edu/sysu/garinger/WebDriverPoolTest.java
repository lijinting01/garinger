package cn.edu.sysu.garinger;

import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.AbandonedConfig;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

@Slf4j
public class WebDriverPoolTest {

  private static final List<String> URLS = new ImmutableList.Builder<String>()
      .add("https://image.baidu.com")
      .add("https://www.baidu.com")
      .add("https://cn.bing.com")
      .add("https://blog.csdn.net/csdnnews/article/details/87871015")
      .add("http://www.cnblogs.com/cnblogsfans/p/5075073.html")
      .add("https://www.cnblogs.com/liuguanglin/p/9397428.html")
      .add("https://www.aliyun.com/")
      .build();
  private WebDriverPool webDriverPool;

  @Before
  public void setUp() {

    final String remoteUrl = "http://192.168.99.104:4444/wd/hub";
    AbandonedConfig abandonedConfig = new AbandonedConfig();
    abandonedConfig.setLogAbandoned(true);
    abandonedConfig.setUseUsageTracking(true);
    abandonedConfig.setRemoveAbandonedOnBorrow(true);

    GenericObjectPoolConfig<WebDriver> objectPoolConfig = new GenericObjectPoolConfig<>();
    objectPoolConfig.setMaxIdle(3);
    objectPoolConfig.setMaxTotal(3);
    objectPoolConfig.setMinIdle(0);

    webDriverPool = new WebDriverPool(
        new RemoteWebDriverFactory(Browser.CHROME, remoteUrl), objectPoolConfig, abandonedConfig);
  }

  @Test
  public void test() {
    URLS.stream()
        .parallel()
        .forEach(this::call);
  }

  private void call(final String url) {
    WebDriver webDriver = null;
    try {
      webDriver = webDriverPool.borrowObject();
      webDriver.get(url);
      String title = webDriver.getTitle();
      log.info("Title : {}, Url: {}", title, url);
    } catch (Exception e) {
      log.error("Class: {}, Message: {}, Url:{}", e.getClass(), e.getMessage(), url);
    } finally {
      Optional.ofNullable(webDriver)
          .ifPresent(webDriverPool::returnObject);
    }
  }

  @After
  public void tearDown() {
    webDriverPool.close();
  }
}
