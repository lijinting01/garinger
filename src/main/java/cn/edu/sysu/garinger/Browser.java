package cn.edu.sysu.garinger;

import java.util.function.Supplier;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;

public enum Browser implements Supplier<Capabilities> {
  CHROME {
    @Override
    public Capabilities get() {
      return new ChromeOptions();
    }
  },
  FIREFOX {
    @Override
    public Capabilities get() {
      return new FirefoxOptions();
    }
  };

}
