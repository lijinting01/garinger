package cn.edu.sysu.garinger;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.RemoteWebDriver;

public class RemoteWebDriverFactory extends WebDriverFactory {

  private Browser browser;

  private String remoteUrl;

  public RemoteWebDriverFactory(Browser browser, String remoteUrl) {
    this.browser = browser;
    this.remoteUrl = remoteUrl;
  }

  @Override
  protected WebDriver newWebDriver() {
    WebDriver webDriver = null;
    try {
      webDriver = new RemoteWebDriver(new URL(remoteUrl), Browser.CHROME.get());
      webDriver.manage()
          .timeouts()
          .implicitlyWait(30, TimeUnit.SECONDS)
          .pageLoadTimeout(30, TimeUnit.SECONDS);
      return webDriver;
    } catch (MalformedURLException e) {
      throw new WebDriverException(e);
    }
  }
}
