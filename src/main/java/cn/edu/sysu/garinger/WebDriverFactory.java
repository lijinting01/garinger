package cn.edu.sysu.garinger;


import java.util.concurrent.TimeUnit;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.openqa.selenium.WebDriver;


/**
 * @author lijinting01
 */
public abstract class WebDriverFactory implements PooledObjectFactory<WebDriver> {

  @Override
  public PooledObject<WebDriver> makeObject() throws Exception {
    WebDriver webDriver = newWebDriver();
    webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    return new DefaultPooledObject<>(webDriver);
  }

  @Override
  public void destroyObject(PooledObject<WebDriver> pooledObject) {
    pooledObject.getObject().quit();
  }

  @Override
  public boolean validateObject(PooledObject<WebDriver> p) {
    return p != null && p.getObject() != null;
  }

  @Override
  public void activateObject(PooledObject<WebDriver> p) throws Exception {
    // nothing to do
  }

  @Override
  public void passivateObject(PooledObject<WebDriver> p) throws Exception {
    // nothing to do
  }

  protected abstract WebDriver newWebDriver();
}
